<?php

function render($view, $data = null, $url = null)
{
	if ($url != null) {
		cekAkses($url);
	}
	$CI = &get_instance();
	$csrf['csrf'] = array(
		'name' => $CI->security->get_csrf_token_name(),
		'hash' => $CI->security->get_csrf_hash()
	);

	// $hasil_menu = tampil_menu($url);
	$menu['menu'] = tampil_menu();
	$menu['url'] = $url;
	// $csrf['jenis_akses'] = $hasil_menu[1];

	if (empty($data)) {
		$data = $csrf;
	} else {
		$data = array_merge($data, $csrf);
	}
	// var_dump($url);die;

	$CI->load->view('template2/header');
	$CI->load->view('template2/sidebar', $menu);
	$CI->load->view($view, $data);
	$CI->load->view('template2/footer');
}

function get_all_menu()
{
	$a = get_instance();
	$menu = $a->db->where("id_sub_menu", 0)->get('tbl_menu')->result();

	$hasil = [];
	foreach ($menu as $key) {
		$hasil[] = [
			"data" => $key,
			"sub_menu" => $a->db->where("id_sub_menu", $key->id_menu)->get('tbl_menu')->result()
		];
	}

	// $menu2 = $a->db->where('id_sub_menu', 0)->where('link !=', '')->get('tbl_menu')->result();
	// foreach ($menu2 as $key) {
	// 	$hasil[] = [
	// 		'data'	=>	$key,
	// 		'sub_menu'	=>	$key
	// 	];
	// }
	return $hasil;
}

function tampil_menu()
{
	$akses = 0;
	$a = get_instance();
	$idne = [];
	$id_menu = $a->db->from("tbl_menu m")->join("tbl_hak_akses h", 'h.id_menu = m.id_menu')->where('id_sub_menu !=', 0)->where("h.id_level", sesi('id_level'))->select("m.id_sub_menu")->group_by('m.id_sub_menu')->order_by('m.id_menu', 'asc')->get()->result();
	// mati(sesi('id_level'));
	foreach ($id_menu as $key) {
		$idne[] = $key->id_sub_menu;
	}

	$menu = $a->db->from('tbl_menu')->where_in('id_menu', $idne)->order_by('urutan', 'asc')->get()->result();


	$hasil = [];
	foreach ($menu as $key) {
		$sub_menu = $a->db->from('tbl_hak_akses h')->join("tbl_menu m", "m.id_menu = h.id_menu")->select("m.*, h.jenis_akses")->where("h.id_level", sesi('id_level'))->where("m.id_sub_menu", $key->id_menu)->order_by('urutan', 'asc')->get()->result();
		$url = [];
		foreach ($sub_menu as $sub) {
			$url[] = $sub->link;
		}
		$hasil[] = [
			"data" 			=> $key,
			"sub_menu"		=> $sub_menu,
			"url"			=>	$url
		];
	}

	// mati($url_cek);
	// die;
	return $hasil;
}

function tampil_menu2($url_cek)
{
	$akses = 0;
	$a = get_instance();
	$idne = [];
	$id_menu = $a->db->from("tbl_menu m")->join("tbl_hak_akses h", 'h.id_menu = m.id_menu')->where('id_sub_menu !=', 0)->where("h.id_level", sesi('id_level'))->select("m.id_sub_menu")->group_by('m.id_sub_menu')->order_by('m.id_menu', 'asc')->get()->result();
	// mati(sesi('id_level'));
	foreach ($id_menu as $key) {
		$idne[] = $key->id_sub_menu;
	}

	$menu = $a->db->from('tbl_menu')->where_in('id_menu', $idne)->order_by('urutan', 'asc')->get()->result();


	$hasil = [];
	foreach ($menu as $key) {
		$sub_menu = $a->db->from('tbl_hak_akses h')->join("tbl_menu m", "m.id_menu = h.id_menu")->select("m.*, h.jenis_akses")->where("h.id_level", sesi('id_level'))->where("m.id_sub_menu", $key->id_menu)->order_by('urutan', 'asc')->get()->result();
		$url = [];
		foreach ($sub_menu as $sub) {
			$url[] = $sub->link;
			// echo "$url_cek dan $sub->link dan $sub->jenis_akses <br>";
			if ($url_cek == $sub->link) {
				$akses = $sub->jenis_akses;
			}
		}
		$hasil[] = [
			"data" 			=> $key,
			"sub_menu"		=> $sub_menu,
			"url"			=>	$url
		];
	}

	// mati($url_cek);
	// die;
	return [$hasil, $akses];
}

function pindah($tujuan)
{
	echo "
		<script>
			location.href = '" . site_url($tujuan) . "';
		</script>
	";
}

function pesan($isi)
{
	echo "
		<script>
			alert('" . $isi . "');
		</script>
	";
}

function mati($var)
{
	echo "<pre>";
	var_dump($var);
	echo "</pre>";
	die;
}

function getPos()
{
	$a = get_instance();
	return $a->input->post(null, TRUE);
}

// Login testing untuk beranda atau template
function isBeranda()
{
	$a = get_instance();
	if (empty(sesi('id_user')) || empty(sesi('fullname'))) {
		pindah("dashboard");
		die;
	}
}

// login cek untuk halaman login
function isLogin()
{
	$a = get_instance();
	if (sesi('id_user') || sesi('fullname')) {
		pindah("beranda");
		die;
	}
}

function cekAkses($url)
{
	$a = get_instance();
	$hasil = $a->db->from('tbl_hak_akses h')->join('tbl_menu m', 'm.id_menu = h.id_menu')->where('m.link', $url)->where('h.id_level', sesi('id_level'))->get()->row();
	if (empty($hasil)) {
		notif("Peringatan", "Anda tidak memiliki hak untuk mengakses link tersebut");
		pindah("beranda");
		die;
	}
}

function inputText($nama)
{
	return `
				<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                  </div>
                </div>  
                
	`;
}

function sesi($var)
{
	$CI = &get_instance();
	return $CI->session->userdata($var);
}

function apiCekLogin()
{
	if (empty(sesi('id_user') || empty(sesi('fullname')))) {
		die;
	}
}

function pos_helper($post, $is_boleh_kosong = true)
{
	$CI = &get_instance();
	$data = htmlspecialchars($CI->input->post($post, TRUE));
	if ($is_boleh_kosong == true) {
		return $data ?? 'kosong';
	} else {
		return $data;
	}
}


function hitung_umur($date)
{
	$saiki = explode('-', date('Y-m-d'));
	$year = 0;
	$date = explode('-', $date);
	if ($date[1] < $saiki[1]) {
	} else if (($date[1] == $saiki[1]) && ($date[0] == $saiki[0])) {
		$year = 1;
	}

	$usia = $saiki[2] - $date[2] - $year;

	if ($usia < 0) {
		$usia = 0;
	}

	return $usia;
}

function rupiah($angka)
{
	$hasil = "RP. " . number_format($angka, 0, '', '.');
	return $hasil;
}

function tanggal_indo($date)
{
	$tanggal = explode('-', $date);
	$bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];


	$output = end($tanggal) . ' ' . $bulan[$tanggal[1] - 1] . ' ' . $tanggal[0];
	return $output;
}

function bulan_depan($date)
{
	$tanggal = explode('-', $date);
	$bulan_berikutnya = $tanggal[1] + 1;

	if ($bulan_berikutnya > 12) {
		$bulan_berikutnya = 1;
		$tanggal[0] = $tanggal[0] + 1;
	}

	$output = $tanggal[0] . '-' . sprintf("%02s", $bulan_berikutnya) . '-' . end($tanggal);
	return $output;
}

function fix_date($date)
{
	$tanggal = explode('-', $date);
	// mati($tanggal);

	if ($tanggal[2] > 30 && (in_array($tanggal[1], [4, 6, 9, 11]))) {
		$tanggal[2] = 30;
	} elseif ($tanggal[2] > 28 && $tanggal[1] == 2) {
		$tanggal[2] = 28;
	}


	$output = $tanggal[0] . '-' . $tanggal[1] . '-' . end($tanggal);
	return $output;
}

function selisih_hari($tgl_awal, $tgl_akhir)
{
	$satu_hari = 24 * 60 * 60;
	$selisih = (strtotime($tgl_akhir) - strtotime($tgl_awal)) / $satu_hari;
	return $selisih;
}

function kode($table, $primary, $tgl, $kode, $panjang)
{
	$panjang_kode = strlen($kode);
	$panjang_tanggal = strlen($tgl);

	$a = get_instance();
	$a->db->like($primary, $tgl);
	$a->db->like($primary, $kode);
	$a->db->order_by($primary, 'desc');
	$a->db->select($primary);
	$hasil = $a->db->get($table, 1)->row_array();

	if (empty($hasil)) {
		$kode_lama = 1;
	} else {
		$kode_lama = substr($hasil[$primary], $panjang_tanggal + $panjang_kode);
		$kode_lama++;
	}
	$kode_baru = $kode . $tgl . sprintf("%0" . $panjang . "s", $kode_lama);
	return $kode_baru;
}

function notif($judul_pesan, $isi_pesan, $background_pesan = "gritter-dark")
{
	$data = "

  <script>
    $.gritter.add({
      title: `$judul_pesan`,
      text: `$isi_pesan`,
      sticky: false,
      class_name: '$background_pesan'
      

    });
  </script>
	";


	$CI = &get_instance();
	$CI->session->set_flashdata("notif", $data);
}



function btn_tambah($url_tambah, $nama)
{
	return "<a href='" . site_url($url_tambah) . "' class='btn btn-primary'> <i class='fa fa-plus-circle'></i> Tambah $nama</a>";
}

function btn_hapus($id, $url, $pesan)
{
	echo "<button idne='$id' tujuane='$url' pesane='$pesan' class='btn btn-danger hapus'><i class='fa fa-trash'></i></button>";
}

function btn_kembali($url, $is_right = false)
{
	echo "<a href='$url'class='btn btn-info " . ($is_right ? 'pull-right' : '') . "'><i class='fa fa-undo'></i> Kembali</a>";
}

function btn_edit($url)
{
	echo "<a href='$url'class='btn btn-warning'><i class='fa fa-pencil'></i></a>";
}


function encrypt($id)
{
	$CI = &get_instance();
	$id = $CI->encryption->encrypt($id);
	$id = urlencode($id);
	$id = str_replace("%", '-', $id);
	return $id;
}

function decrypt($id)
{
	$CI = &get_instance();
	$id = str_replace("-", '%', $id);
	$id = urldecode($id);
	$id = $CI->encryption->decrypt($id);
	return $id;
}


function load_datatabel()
{
	echo '
	<link rel="stylesheet" href="' . base_url() . 'template2/datatable/css/dataTables.bootstrap.min.css">
<script src="' . base_url() . 'template2/datatable/jquery.dataTables.min.js"></script>
<script src="' . base_url() . 'template2/datatable/js/dataTables.bootstrap.min.js"></script>
	';
}
