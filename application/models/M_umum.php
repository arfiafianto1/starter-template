<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_umum extends CI_Model
{

	function hak_akses_edit($id_level, $pos)
	{
		$this->db->delete('tbl_hak_akses', ['id_level' => $id_level]);

		foreach ($pos['sub_menu'] as $key) {
			$this->db->insert('tbl_hak_akses', ['id_level' => $id_level, 'id_menu' => $key]);
		}
	}
}

/* End of file M_umum.php */
/* Location: ./application/models/M_umum.php */