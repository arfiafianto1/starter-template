<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{

    public function index()
    {
        $data['menu'] = $this->db->from('tbl_menu')->where('id_sub_menu', 0)->get()->result();
        $data['sub_menu'] = $this->db->from('tbl_menu')->where('id_sub_menu !=', 0)->get()->result();
        render("menu/index", $data, 'menu');
    }

    function add_menu()
    {
        if ($this->input->post() != null) {
            $data = [
                'nama_menu'         =>      pos_helper('nama_menu'),
                'link'              =>      '',
                'icon'              =>      pos_helper('icon'),
                'urutan'            =>      pos_helper('urutan'),
                'id_sub_menu'       =>      0
            ];

            $this->db->insert('tbl_menu', $data);
            $data = $this->db->affected_rows();
            ($data == 1) ? notif("Pemberitahuan", "Data Menu berhasil ditambah") : notif("Peringatan", "Data menu gagal ditambah");
        }

        render('menu/add_menu', null, 'menu');
    }

    function add_sub_menu()
    {
        if ($this->input->post() != null) {
            $data = [
                'nama_menu'         =>      pos_helper('nama_menu'),
                'link'              =>      pos_helper('link'),
                'icon'              =>      pos_helper('icon'),
                'urutan'            =>      pos_helper('urutan'),
                'id_sub_menu'       =>      pos_helper('id_sub_menu')
            ];

            $this->db->insert('tbl_menu', $data);
            $data = $this->db->affected_rows();
            ($data == 1) ? notif("Pemberitahuan", "Data Menu berhasil ditambah") : notif("Peringatan", "Data menu gagal ditambah");
        }
        $datae['menu'] = $this->db->where('link', '')->get('tbl_menu')->result();
        render('menu/add_sub_menu', $datae, 'menu');
    }

    function hapus()
    {
        $id = pos_helper('id');
        $id = decrypt($id);
        $cek = $this->db->from('tbl_menu')->where('id_sub_menu', $id)->get()->num_rows();
        if ($cek == 0) {
            $this->db->delete('tbl_menu', ['id_menu' => $id]);
            $status = $this->db->affected_rows();
            ($status == 1) ? notif("Pemberitahuan", "Data berhasil dihapus") : notif("Peringatan", "Data gagal dihapus");
        } else {
            notif("Peringatan", "Menu yang dihapus memiliki anak menu. Tidak boleh dihapus");
        }


        redirect('menu');
    }

    public function edit($id)
    {
        $id = decrypt($id);
        if ($this->input->post() != null) {
            $edit = [
                'nama_menu'     =>      pos_helper('nama_menu'),
                'icon'          =>      pos_helper('icon'),
                'urutan'        =>      pos_helper('urutan')
            ];
            $this->db->update('tbl_menu', $edit, ['id_menu' => $id]);
            $hasil = $this->db->affected_rows();
            ($hasil == 1) ? notif("Pemberitahuan", "Menu Berhasil diubah") : notif("Peringatan", "Menu gagal diubah");
        }
        $data['menu'] = $this->db->get_where('tbl_menu', ['id_menu' => $id])->row();
        $data['id'] = $id;
        render("menu/edit_menu", $data, 'menu');
    }

    public function edit_sub_menu($id)
    {
        $id = decrypt($id);
        if ($this->input->post() != null) {
            $edit = [
                'nama_menu'     =>      pos_helper('nama_menu'),
                'icon'          =>      pos_helper('icon'),
                'urutan'        =>      pos_helper('urutan'),
                'link'          =>      pos_helper('link')
            ];
            $this->db->update('tbl_menu', $edit, ['id_menu' => $id]);
            $hasil = $this->db->affected_rows();
            ($hasil == 1) ? notif("Pemberitahuan", "Sub Menu Berhasil diubah") : notif("Peringatan", "Sub Menu gagal diubah");
        }
        $data['menu'] = $this->db->get_where('tbl_menu', ['id_menu' => $id])->row();
        $data['menue'] = $this->db->where('id_sub_menu', 0)->get('tbl_menu')->result();
        $data['id'] = $id;

        render("menu/edit_submenu", $data, 'menu');
    }
}

/* End of file Menu.php */
