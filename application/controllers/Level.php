<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Level extends CI_Controller
{

    public function index()
    {
        $data['level'] = $this->db->get('tbl_level')->result();
        render("level/index", $data, 'level');
    }

    public function tambah()
    {
        if ($this->input->post() != null) {
            $data = [
                'nama_level'         =>      pos_helper('nama_level'),
            ];

            $this->db->insert('tbl_level', $data);
            $data = $this->db->affected_rows();
            ($data == 1) ? notif("Pemberitahuan", "Data level berhasil ditambah") : notif("Peringatan", "Data level gagal ditambah");
        }
        render('level/tambah', null, 'level');
    }

    public function edit($id)
    {
        $id = decrypt($id);
        if ($this->input->post() != null) {
            $edit = [
                'nama_level'     =>      pos_helper('nama_level'),
            ];
            $this->db->update('tbl_level', $edit, ['id_level' => $id]);
            $hasil = $this->db->affected_rows();
            ($hasil == 1) ? notif("Pemberitahuan", "level Berhasil diubah") : notif("Peringatan", "level gagal diubah");
        }
        $data['level'] = $this->db->get_where('tbl_level', ['id_level' => $id])->row();
        $data['id'] = $id;
        render("level/edit", $data, 'level');
    }

    public function hapus()
    {
        $id = pos_helper('id');
        $id = decrypt($id);
        $cek = $this->db->get_where('tbl_user', ['id_level' => $id])->num_rows();
        if ($cek == 0) {
            $this->db->delete('tbl_level', ['id_level' => $id]);
            $status = $this->db->affected_rows();
            ($status == 1) ? notif("Pemberitahuan", "Level berhasil dihapus") : notif("Peringatan", "Level gagal dihapus");
        } else {
            notif("Peringatan", "Masih ada user yang menjabat level tersebut. Tidak boleh dihapus");
        }


        redirect('level');
    }
}

/* End of file Level.php */
