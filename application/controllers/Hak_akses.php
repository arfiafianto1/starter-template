<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hak_akses extends CI_Controller
{

	public function index()
	{
		cekAkses('hak_akses');
		$data['level'] = $this->db->get('tbl_level')->result();
		render('hak_akses/hak_akses', $data, 'hak_akses');
	}

	public function edit($value = null)
	{
		cekAkses('hak_akses');
		$pos = $this->input->post(null, TRUE);
		$value = decrypt($value);
		$level = $this->db->get_where('tbl_level', ['id_level' => $value])->row();
		if (empty($value) || empty($level)) {
			pindah("hak_akses");
		} elseif (!empty($pos)) {
			$this->load->model('M_umum');
			$this->M_umum->hak_akses_edit($value, $pos);
			// mati($pos);
			// $this->db->delete('tbl_hak_akses', ['id_level' => $value]);

			// foreach ($pos['akses'] as $key => $akses) {
			// 	echo "ID Menu = $key dan akses = $akses <br>";

			// 	if ($akses != 0) {
			// 		$this->db->insert('tbl_hak_akses', [
			// 			'id_level'		=>	$value,
			// 			'id_menu'		=>	$key,
			// 			'jenis_akses'	=>	$akses
			// 		]);
			// 	}
			// }
			// die;
			// pesan("Hak Akses berhasil diubah");
			// pindah('hak_akses');
			notif("Pemberitahuan", "Hak AKses berhasil diubah");

			redirect('hak_akses');
		} else {
			// mati($pos);
		}

		$data['hak_akses'] = $this->getHakAkses($value);
		$data['menu'] = get_all_menu();
		$data['level'] = $level;
		// $data['akses'] = [
		// 	0	=>	"No Access",
		// 	1	=>	"View Only",
		// 	2	=>	"View, add",
		// 	3	=>	"View, add, edit",
		// 	4	=>	"View, add, edit, delete"
		// ];
		// var_dump($data);die;
		// mati($data['hak_akses']);
		render("hak_akses/edit_hak_akses", $data, 'hak_akses');
	}





	// Private Function



	private function getHakAkses($id_level)
	{
		$this->db->from('tbl_level l');
		$this->db->join('tbl_hak_akses h', 'h.id_level = l.id_level');
		$this->db->join('tbl_menu m', 'm.id_menu = h.id_menu');
		$this->db->select('m.id_menu, h.jenis_akses');
		$this->db->where('h.id_level', $id_level);
		$hasil = $this->db->get()->result();
		$output = [];
		foreach ($hasil as $key) {
			$output[] = $key->id_menu;
			// $output['akses'][$key->id_menu] = $key->jenis_akses;
		}

		return $output;
	}

	private function getHakAkses2($id_level)
	{
		$this->db->from('tbl_level l');
		$this->db->join('tbl_hak_akses h', 'h.id_level = l.id_level');
		$this->db->join('tbl_menu m', 'm.id_menu = h.id_menu');
		$this->db->select('m.id_menu, h.jenis_akses');
		$this->db->where('h.id_level', $id_level);
		$hasil = $this->db->get()->result();
		$output = [];
		foreach ($hasil as $key) {
			$output['id_menu'][] = $key->id_menu;
			$output['akses'][$key->id_menu] = $key->jenis_akses;
		}

		return $output;
	}
}

/* End of file Hak_akses.php */
/* Location: ./application/controllers/Hak_akses.php */