<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Dashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("<small class='text-danger'>", "</small>");
	}

	public function index()
	{
		$kode = $this->config->item("kode_app");
		$pos = getPos();
		if (empty($pos)) {
		} else {
			$status = $this->cekLogin(elements(['username', 'password'], $pos, 0));

			if ($status['status'] == false) {
				pesan("username dan password salah");
			} else {

				$data = array(
					'id_user' => $status['data']->id_user,
					'fullname' => $status['data']->fullname,
					'id_level' => $status['data']->id_level,
					'img'		=> $status['data']->img,
					"level"	=>	$this->db->get_where('tbl_level', ['id_level' => $status['data']->id_level])->row()->nama_level
				);
				$this->session->set_userdata($data);
				pesan("selamat datang di MyPOS " . $data['fullname']);
				pindah("beranda");
			}
		}
		// var_dump($this->session->userdata());
		$this->load->view('auth/login2');
	}

	public function beranda()
	{
		render("welcome_message");
	}

	public function logout()
	{
		$this->session->sess_destroy();
		pindah('dashboard');
	}

	public function profil()
	{
		$img = $this->db->select('img')->where('id_user', sesi('id_user'))->get('tbl_user')->row()->img;

		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|callback_username_check');

		if ($this->form_validation->run() == false) {
		} else {
			$pos = getPos();
			$update['username'] = $pos['username'];
			$update['alamat'] = $pos['alamat'];
			$update['no_telp'] = $pos['no_telp'];
			$update['fullname'] = $pos['fullname'];
			$update['email'] = $pos['email'];
			if (!empty($pos['password'])) {
				$update['password'] = $this->encryption->ecnrypt($pos['password']);
			}


			$config['upload_path'] = FCPATH . '/img/';
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size']  = '1000';
			$config['max_width']  = '1024';
			$config['max_height']  = '1068';

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('img')) {
				$data['eror'] = $this->upload->display_errors();
			} else {
				$update['img'] = $this->upload->data('file_name');
				if ($img != 'user.jpg') {
					unlink(FCPATH . '/img/' . $img);
				}

				$this->session->set_userdata('img', $this->upload->data('file_name'));
			}

			$this->session->set_userdata('fullname', $pos['fullname']);
			$this->db->update('tbl_user', $update, ['id_user' => sesi('id_user')]);


			$status = $this->db->affected_rows();
			if ($status > 0) {
				notif("Pemberitahuan", "profil anda berhasil diubah");
			} else {
				notif("Peringatan", "Profil gagal diubah");
			}
		}

		// notif('Selamat Datang', "Apa Kabarmu Hari ini", "bg-danger");
		$data['profil'] = $this->db->where('id_user', sesi('id_user'))->select('fullname,username,email,alamat,no_telp,img')->get('tbl_user')->row();
		render('profil', $data);
	}





	// <=======================================================>

	private function cekLogin($array)
	{
		$username = $array['username'];
		$password = $array['password'];

		$hasil = $this->db->get_where('tbl_user', ['username' => $username])->row();
		$output['status'] = false;
		if (empty($hasil)) {
		} elseif ($password != $this->encryption->decrypt($hasil->password)) {
		} else {
			$output['status'] = true;
			$output['data'] = $hasil;
		}

		return $output;
	}

	function username_check($isi)
	{
		$hasil = $this->db->from('tbl_user')->select('username')->where('id_user !=', sesi('id_user'))->where("username", $isi)->get()->result();
		$this->form_validation->set_message('username_check', "username sudah dipakai, silahkan pilih yang lain");
		if (empty($hasil)) {
			return true;
		} else {
			return false;
		}
	}
}
