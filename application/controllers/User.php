<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("<small class='text-danger'>", "</small>");
	}


	public function index()
	{
		cekAkses('user');
		$data['level'] = $this->db->get('tbl_level')->result();
		$data['user'] = $this->db->where_not_in('id_user', [sesi('id_user')])->from('tbl_user u')->join('tbl_level l', 'l.id_level = u.id_level')->select('u.*,l.nama_level')->get()->result();
		render('user/lihatUser', $data, 'user');
	}

	public function tambah_user()
	{
		$pos = getPos();
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|is_unique[tbl_user.username]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[8]|matches[password]');
		if ($this->form_validation->run() == FALSE) {
		} else {
			$update = array(
				'username' => $pos['username'],
				'password' => $this->encryption->encrypt($pos['password']),
				'id_level' => $pos['id_level'],
				'fullname'	=>	$pos['fullname'],
				'email'	=>	$pos['email'],
				'alamat'	=>	$pos['alamat'],
				'no_telp'	=>	$pos['no_telp'],
			);
			$this->db->insert('tbl_user', $update);
			$hasil = $this->db->affected_rows();
			if ($hasil == 1) {
				notif("Pemberitahuan", "Data User berhasil ditambah");

				redirect('user', 'refresh');
			} else {
				notif("Peringatan", "Data User gagal ditambah");
			}
		}

		$data['level'] = $this->db->get('tbl_level')->result();
		render('user/addUser', $data, 'user');
	}

	function toggle($id_user = null)
	{
		$id_user = decrypt($id_user);
		$status = $this->db->get_where('tbl_user', ['id_user' => $id_user])->row();
		if (empty($status)) {
			pindah('user');
		} else {
			$status = $status->status;
		}

		// mati($status);
		if ($status == '1') {
			$baru = ['status' => '0'];
		} else {
			$baru = ['status' => '1'];
		}
		// mati($baru);
		// mati(
		$this->db->update('tbl_user', $baru, ['id_user' => $id_user]);
		// );
		// dbs
		$hasil = $this->db->affected_rows();
		// mati($hasil);die;
		($hasil == 1) ? notif("Pemberitahuan", "Data Berhasil Diubah") : notif("Peringatan", "Data gagal diubah");
		pindah('user');
	}

	public function updateLevel()
	{
		$pos = $this->input->post(null, TRUE);

		$this->db->update('tbl_user', ['id_level' => $pos['id_level']], ['id_user' => decrypt($pos['id_user'])]);
		echo $this->db->affected_rows();
	}

	public function tes()
	{
		$a = "Testing";
		$a = $this->encryption->encrypt($a);
		$a = urlencode($a);
		// $a = str_replace("/", '_', $a);
		// $a = str_replace("+", '-', $a);
		echo $a . "<br>";

		// $a = str_replace("-", '+', $a);
		$a = urldecode($a);
		$a = $this->encryption->decrypt($a);
		echo $a;
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */