 <aside class="main-sidebar">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
     <!-- Sidebar user panel -->
     <div class="user-panel">
       <div class="pull-left image">
         <img src="<?= base_url("template/img/" . sesi('img')) ?>" width='160px' height='160px' class="img-circle" alt="User Image">
       </div>
       <div class="pull-left info">
         <p><?php echo sesi('fullname'); ?></p>
         <p><?= sesi('level') ?></p>
       </div>
     </div>

     <!-- sidebar menu: : style can be found in sidebar.less -->
     <ul class="sidebar-menu" data-widget="tree">

       <li class="<?= empty($url) ? 'active' : '' ?>">
         <a href="<?php echo site_url('beranda') ?>">
           <i class="fa fa-dashboard"></i> <span>Beranda</span>
         </a>
       </li>
       <?php foreach ($menu as $key) : ?>
         <li class="treeview <?= in_array($url, $key['url']) ? 'active menu-open' : '' ?>">
           <a href="#">
             <i class="<?php echo $key['data']->icon ?>"></i>
             <span><?php echo $key['data']->nama_menu ?></span>
             <span class="pull-right-container">
               <i class="fa fa-angle-left pull-right"></i>
             </span>
           </a>
           <ul class="treeview-menu">
             <?php foreach ($key['sub_menu'] as $sub) : ?>
               <li class="<?= $url == $sub->link ? "active" : ''; ?>"><a href="<?php echo site_url($sub->link) ?>"><i class="<?php echo $sub->icon ?>"></i> <?php echo $sub->nama_menu ?></a></li>
             <?php endforeach ?>
           </ul>
         </li>
       <?php endforeach ?>

     </ul>
   </section>
   <!-- /.sidebar -->
 </aside>