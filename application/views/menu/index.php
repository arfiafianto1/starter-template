<?= load_datatabel() ?>
<section id="main-content">
    <section class="wrapper site-min-height">


        <div class="row mt">
            <div class="form-panel  panel-success">
                <div class="panel-heading">
                    <h2>Daftar Menu</h2>
                    <p>Halaman ini menampikan daftar menu</p>
                    <?= btn_tambah('menu/add_menu', "Menu") ?>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Menu</th>
                                    <th>Icon</th>
                                    <th>Link</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($menu as $m => $value) : ?>
                                    <tr>
                                        <td><?= $m + 1 ?></td>
                                        <td><?= $value->nama_menu ?></td>
                                        <td><?= $value->icon ?></td>
                                        <td><?= $value->link ?></td>
                                        <td>
                                            <a href="<?= base_url('menu/edit/' . encrypt($value->id_menu)) ?>" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <button idne="<?= encrypt($value->id_menu) ?>" tujuane="<?= base_url('menu/hapus') ?>" pesane="Menu <?= $value->nama_menu ?>" class="btn btn-danger hapus"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-panel  panel-success">
                <div class="panel-heading">
                    <h2>Daftar Menu</h2>
                    <p>Halaman ini menampikan daftar menu dan submenu</p>
                    <?= btn_tambah('menu/add_sub_menu', "Submenu") ?>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Menu</th>
                                    <th>Icon</th>
                                    <th>Link</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sub_menu as $m => $value) : ?>
                                    <tr>
                                        <td><?= $m + 1 ?></td>
                                        <td><?= $value->nama_menu ?></td>
                                        <td><?= $value->icon ?></td>
                                        <td><?= $value->link ?></td>
                                        <td>
                                            <a href="<?= base_url('menu/edit_sub_menu/' . encrypt($value->id_menu)) ?>" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <?= btn_hapus(encrypt($value->id_menu), base_url('menu/hapus'), "Menu " . $value->nama_menu) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<script>
    $(".table").DataTable();
</script>