<section id="main-content">
    <section class="wrapper site-min-height">


        <div class="row mt">
            <div class="form-panel  panel-success">
                <div class="panel-heading">
                    <h2>Tambah Menu Baru</h2>
                </div>
                <div class="panel-body">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="a">Pilih Menu</label>
                                    <select name="id_sub_menu" id="a" class="form-control">
                                        <?php foreach ($menu as $m) : ?>
                                            <option value="<?= $m->id_menu ?>"><?= $m->nama_menu ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nama_menu">Nama Menu</label>
                                    <input type="text" name="nama_menu" class="form-control" id="nama_menu" required>
                                </div>
                                <div class="form-group">
                                    <label for="link">Link Menu</label>
                                    <input type="text" name="link" class="form-control" id="link" required>
                                </div>
                                <div class="form-group">
                                    <label for="icon">Icon Menu</label>
                                    <input type="text" name="icon" class="form-control" id="icon" required>
                                </div>
                                <div class="form-group">
                                    <label for="urutan">Urutan</label>
                                    <input type="number" name="urutan" class="form-control" id="urutan" required>
                                    <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>">
                                </div>
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Simpan Menu</button>
                                <?= btn_kembali(base_url('menu')) ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</section>