<section id="main-content">
  <section class="wrapper site-min-height">
    <h3><i class="fa fa-angle-right"></i> profil Saya</h3>
    <p></p>
    <div class="row">
      <div class="form-panel panel-primary">
        <div class="panel-heading">
          Berisi informasi profil anda
        </div>
        <div class="panel-body">
          <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="fullname" class="col-sm-2 col-md-5 control-label">Nama Lengkap</label>
                <div class="col-sm-10 col-md-7">
                  <input type="text" class="form-control" name="fullname" id="fullname" placeholder="" value="<?php echo $profil->fullname ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="alamat" class="col-sm-2 col-md-5 control-label">Alamat</label>
                <div class="col-sm-10 col-md-7">
                  <input type="text" class="form-control" name="alamat" id="alamat" placeholder="" value="<?php echo $profil->alamat ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-2 col-md-5 control-label">Email</label>
                <div class="col-sm-10 col-md-7">
                  <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo $profil->email ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="no_telp" class="col-sm-2 col-md-5 control-label">No Telp</label>
                <div class="col-sm-10 col-md-7">
                  <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="" value="<?php echo $profil->no_telp ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="username" class="col-sm-2 col-md-5 control-label">Username</label>
                <div class="col-sm-10 col-md-7">
                  <input type="text" class="form-control" name="username" id="username" placeholder="" value="<?php echo $this->input->post('username') ?? $profil->username ?>">
                  <?php echo form_error('username') ?>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-sm-2 col-md-5 control-label">Password</label>
                <div class="col-sm-10 col-md-7">
                  <input type="password" class="form-control" name="password" id="password" placeholder="">
                  <small>Biarkan kosong jika tidak ingin mengubah password</small>
                </div>
              </div>

            </div>
            <div class="col-md-6 col-sm-12 align-center">
              <img src="<?php echo base_url('img/' . $profil->img) ?>" width="200" height="200">
              <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
              <br><br>
              <div class="form-group">
                <label for="img" class="col-sm-2 col-md-2 control-label">Ganti Gambar</label>
                <div class="col-sm-10 col-md-10">
                  <input type="file" class="form-control" name="img" id="img" placeholder="">
                  <?php if (!empty($eror)) {
                    echo "$eror";
                  } ?>
                </div>
              </div>
            </div>
            <button class="btn btn-success " type="submit">Simpan Perubahan</button>
          </form>
        </div>
      </div>
    </div>
  </section>

</section>