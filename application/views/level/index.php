<section id="main-content">
    <section class="wrapper site-min-height">


        <div class="row mt">
            <div class="form-panel  panel-success">
                <div class="panel-heading">
                    <h2>Level Manajemen</h2>
                    <p>Halaman ini digunakan untuk mengatur level - level dalam perusahaan</p>
                    <?= btn_tambah('level/tambah', 'Tambah Level Baru') ?>
                </div>
                <div class="panel-body">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Level</th>
                                    <th>Tombol</th>
                                </tr>
                                <?php $no = 1;
                                foreach ($level as $key) : ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo $key->nama_level ?></td>
                                        <td>
                                            <?= btn_edit(base_url('level/edit/' . encrypt($key->id_level))) ?>
                                            <?= btn_hapus(encrypt($key->id_level), base_url('level/hapus'), "Level " . $key->nama_level) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</section>