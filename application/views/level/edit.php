<section id="main-content">
    <section class="wrapper site-min-height">


        <div class="row mt">
            <div class="form-panel  panel-success">
                <div class="panel-heading">
                    <h2>Edit Level</h2>
                </div>
                <div class="panel-body">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="nama_level">Nama Level</label>
                                    <input type="text" name="nama_level" class="form-control" id="nama_level" required value="<?= $level->nama_level ?>">
                                </div>
                                <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>">
                                <input type="hidden" name="id_level" value="<?= encrypt($id) ?>">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan Level</button>
                                <?= btn_kembali(base_url('level')) ?>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>

</section>