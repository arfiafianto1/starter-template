  <section id="main-content">
    <section class="wrapper site-min-height">
      <h3><i class="fa fa-angle-right"></i> Edit Hak Akses</h3>
      <p></p>
      <div class="row mt">
        <div class="col-lg-12">
          <div class="form-panel">
            <form action="" method="POST">
              <div class="table-responsive">
                <table class="table table-striped ">
                  <?php foreach ($menu as $key) : ?>
                    <tr>
                      <td><?php echo $key['data']->nama_menu ?></td>
                      <td>
                        <ol>

                          <?php foreach ($key['sub_menu'] as $sub) : ?>
                            <li><input type="checkbox" name="sub_menu[]" value="<?php echo $sub->id_menu ?>" <?php echo in_array($sub->id_menu, $hak_akses) ? "checked" : '' ?>> <?php echo $sub->nama_menu ?></li>
                          <?php endforeach ?>

                        </ol>

                      </td>
                    </tr>
                  <?php endforeach ?>
                  <tr>
                    <td></td>
                    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <td>
                      <button type="submit" class="btn btn-success">Simpan</button>
                      <?= btn_kembali(base_url('hak_akses')) ?>
                    </td>
                  </tr>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

  </section>