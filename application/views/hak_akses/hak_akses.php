<section id="main-content">
  <section class="wrapper site-min-height">


    <div class="row mt">
      <div class="form-panel  panel-success">
        <div class="panel-heading">
          <h2>Hak Akses</h2>
          <p>Halaman ini digunakan untuk setting menu - menu yang boleh diakses oleh level tertentu</p>
        </div>
        <div class="panel-body">
          <div class="col-12">
            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Nama Level</th>
                  <th>Tombol</th>
                </tr>
                <?php $no = 1;
                foreach ($level as $key) : ?>
                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $key->nama_level ?></td>
                    <td>
                      <!-- <a href="<?php echo site_url('hak_akses/view/' . $key->id_level) ?>" class="btn  btn-success "><i class="fa fa-eye"></i> Lihat</a> -->
                      <a href="<?php echo site_url('hak_akses/edit/' . encrypt($key->id_level)) ?>" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                    </td>
                  </tr>
                <?php endforeach ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</section>