  <?php
    function cek_jenis_akses($id_menu, $hak_akses, $akses)
    {
        if (!empty($hak_akses)) {
            if (in_array($id_menu, $hak_akses['id_menu'])) {
                if ($hak_akses['akses'][$id_menu] == $akses) {
                    return 'selected';
                }
            }
        }
    }

    ?>
  <section id="main-content">
      <section class="wrapper site-min-height">
          <h3><i class="fa fa-angle-right"></i> Edit Hak Akses</h3>
          <p></p>
          <div class="row mt">
              <div class="col-lg-12">
                  <div class="form-panel">
                      <form action="" method="POST">
                          <div class="table-responsive">
                              <table class="table table-striped table-bordered text-center">
                                  <thead>
                                      <tr>
                                          <th>Nama Menu</th>
                                          <th>Nama Sub Menu</th>
                                          <th>Opsi</th>
                                      </tr>
                                  </thead>
                                  <?php foreach ($menu as $m) : ?>
                                      <tr>
                                          <td><?= $m['data']->nama_menu ?></td>
                                          <td></td>
                                          <td></td>
                                      </tr>
                                      <?php foreach ($m['sub_menu'] as $s) : ?>
                                          <tr>
                                              <td></td>
                                              <td><?= $s->nama_menu ?></td>
                                              <td>
                                                  <select name="akses[<?= $s->id_menu ?>]" class="form-control">
                                                      <?php foreach ($akses as $a => $b) : ?>
                                                          <option value="<?= $a ?>" <?= cek_jenis_akses($s->id_menu, $hak_akses, $a) ?>><?= $b ?></option>
                                                      <?php endforeach ?>
                                                  </select>

                                              </td>
                                          </tr>
                                      <?php endforeach ?>
                                  <?php endforeach ?>

                              </table>
                              <input type="hidden" name="<?= $csrf['name'] ?>" id="" value="<?= $csrf['hash'] ?>">
                              <button type="submit" class="btn btn-success">Simpan</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </section>

  </section>