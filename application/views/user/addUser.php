<section id="main-content">
  <section class="wrapper site-min-height">
    <h3><i class="fa fa-user-plus"></i>Tambah Data Pengguna</h3>
    <div class="row mt">
      <div class="form-panel">
        <div class="panel-body">
          <form action="" method="POST" id="my_form">
            <div class="col-md-6">
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" id="username" name="username" class="form-control" required value="<?= set_value('username') ?>">
                <?= form_error('username') ?>
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" class="form-control" value="<?= set_value('password') ?>" required>
                <?= form_error('password') ?>
              </div>
              <div class="form-group">
                <label for="confirm_password">Ulangi Password</label>
                <input type="password" id="confirm_password" name="confirm_password" class="form-control" required value="<?= set_value('confirm_password') ?>">
                <?= form_error('confirm_password') ?>
              </div>
              <div class="form-group">
                <label for="id_level">Jabatan</label>
                <select id="id_level" name="id_level" class="form-control" required>
                  <?php foreach ($level as $l) : ?>
                    <option value="<?= $l->id_level ?>"><?= $l->nama_level ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="fullname">Nama Lengkap</label>
                <input type="text" id="fullname" name="fullname" class="form-control" required value="<?= set_value('fullname') ?>">

              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" required value="<?= set_value('email') ?>">

              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" id="alamat" name="alamat" class="form-control" required value="<?= set_value('alamat') ?>">

              </div>
              <div class="form-group">
                <label for="no_telp">Nomor Telepon</label>
                <input type="number" id="no_telp" name="no_telp" class="form-control" required value="<?= set_value('no_telp') ?>">
                <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>">
              </div>
            </div>
            <button type="submit" class="btn btn-success pull-right">Simpan User</button>
            <?= btn_kembali(base_url('user')) ?>
          </form>
        </div>
      </div>

    </div>
    </div>
  </section>

</section>



<script>

</script>