<section id="main-content">
  <section class="wrapper site-min-height">
    <h3 style="display: flex; justify-content:space-between;">
      <span> <i class="fa fa-angle-right"></i> Daftar User</span>
      <span><?= btn_tambah('user/tambah_user', "User") ?></span>
    </h3>

    <div class="row">
      <?php foreach ($user as $u) : ?>
        <div class="col-md-4 mb-2">
          <!-- WHITE PANEL - TOP USER -->
          <div class="white-panel">
            <div class="white-header">
              <h5> <b><?= $u->nama_level ?></b></h5>
            </div>
            <p><img src="<?= base_url('img/' . $u->img) ?>" class="img-circle" width="50"></p>
            <p><b><?= $u->fullname ?></b></p>
            <ul class="list-group">
              <li class="list-group-item"><?= $u->email ?></li>
              <li class="list-group-item"><?= $u->alamat ?></li>
              <li class="list-group-item"><?= $u->no_telp ?></li>
              <li class="list-group-item" style="display: flex; justify-content:space-between;">
                <button type="button" class="btn btn-success ubah" idne="<?= encrypt($u->id_user) ?>" levele="<?= $u->id_level ?>">Ubah Jabatan</button>
                <a href="<?= site_url('user/toggle/' . encrypt($u->id_user)) ?>" class="btn btn-warning"><?= ($u->status == '1') ? "Nonaktifkan" : "Aktifkan"; ?></a>
              </li>
            </ul>
          </div>
        </div>
      <?php endforeach ?>
    </div>
  </section>

</section>


<div class="modal fade" id="modal-info">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Data User</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="frmUpdateLevel">
          <label for="selecte">Pilih Level</label>
          <select name="id_level" id="selecte" class="form-control">
            <?php foreach ($level as $key) : ?>
              <option value="<?php echo $key->id_level ?>"><?php echo $key->nama_level ?></option>
            <?php endforeach ?>
            <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
            <input type="hidden" name="id_user" id="id_usere">
          </select>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="btnUpdateLevel">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
  $(document).on('click', '.ubah', function(argument) {
    $('#selecte').val($(this).attr('levele'));
    $('#id_usere').val($(this).attr('idne'));
    $("#modal-info").modal('show');
  });

  $('#btnUpdateLevel').click(function(argument) {
    $.ajax({
      url: "<?= site_url('user/updateLevel') ?>",
      method: 'POST',
      data: $('#frmUpdateLevel').serialize(),
      dataType: 'text',
      success: function(data) {
        if (data == 1) {
          pesan("Pemberitahuan", "Data User berhasil diubah");
          $("#modal-info").modal('hide');
          setTimeout(() => {
            location.reload();
          }, 2000);
        } else {
          pesan("Peringatan", "Data user gagal diubah");
        }
      }
    });
  })
</script>