<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">Apakah anda yakin ingin menghapus <b id="pesan_hapus"></b></div>
                <form action="" method="POST" id="form_hapus">
                    <input type="hidden" name="id" id="id_hapus">
                    <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Yes</button></form>
            </div>
        </div>
    </div>
</div>
<footer class="site-footer">
    <div class="text-center">
        <p>
            &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
            <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
            Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="blank.html#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>
<!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?= base_url() ?>template2/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>template2/lib/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?= base_url() ?>template2/lib/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="<?= base_url() ?>template2/lib/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?= base_url() ?>template2/lib/jquery.scrollTo.min.js"></script>
<script src="<?= base_url() ?>template2/lib/jquery.nicescroll.js" type="text/javascript"></script>
<!--common script for all pages-->
<script src="<?= base_url() ?>template2/lib/common-scripts.js"></script>
<!--script for this page-->
<script>
    $(document).on('click', '.hapus', function() {
        const id = $(this).attr('idne');
        const tujuan = $(this).attr('tujuane');
        const pesan = $(this).attr('pesane');
        $("#id_hapus").val(id);
        $("#form_hapus").attr('action', tujuan);
        $("#pesan_hapus").text(pesan);
        $("#myModal").modal('show');
    });
</script>
</body>

</html>