 <aside>
     <div id="sidebar" class="nav-collapse ">
         <!-- sidebar menu start-->
         <ul class="sidebar-menu" id="nav-accordion">
             <p class="centered"><a href="<?= site_url('dashboard/profil') ?>"><img src="<?= base_url('img/' . sesi('img')) ?>" class="img-circle" width="80"></a></p>
             <h5 class="centered"><?= sesi('fullname') ?></h5>
             <li class="mt">
                 <a href="<?= base_url() ?>">
                     <i class="fa fa-dashboard"></i>
                     <span>Dashboard</span>
                 </a>
             </li>
             <?php foreach ($menu as $m) : ?>
                 <li class="sub-menu">
                     <a href="javascript:;" <?= (in_array($url, $m['url']) ? "class='active'" : '') ?>>
                         <i class="<?= $m['data']->icon ?>"></i>
                         <span><?= $m['data']->nama_menu ?></span>
                     </a>
                     <ul class="sub">
                         <?php foreach ($m['sub_menu'] as $s) : ?>
                             <li <?= ($url == $s->link) ? "class='active'" : '' ?>><a href="<?= site_url($s->link) ?>"><i class="<?= $s->icon ?>"></i> <?= $s->nama_menu ?></a></li>
                         <?php endforeach ?>
                     </ul>
                 </li>

             <?php endforeach ?>
         </ul>
         <!-- sidebar menu end-->
     </div>
 </aside>