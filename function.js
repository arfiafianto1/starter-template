function jadikan_rupiah(angka) {
	let angka2 = 0;

	if (angka < 0) {
		angka2 = Math.abs(angka);
	} else {
		angka2 = angka;
	}

	var numeber_string = angka2.toString(),
		sisa = numeber_string.length % 3,
		rupiah = numeber_string.substr(0, sisa),
		ribuan = numeber_string.substr(sisa).match(/\d{3}/g);

	if (ribuan) {
		separator = sisa ? "." : "";

		rupiah += separator + ribuan.join(".");
	}

	if (angka < 0) {
		return "Rp. -" + rupiah;
	} else {
		return "Rp. " + rupiah;
	}
}

function hitung_umur(lahir) {
	let saiki = new Date();
	let ultah = new Date(lahir);
	let year = 0;
	if (saiki.getMonth() < ultah.getMonth()) {
	} else if (
		saiki.getMonth() == ultah.getMonth() &&
		saiki.getDate() < ultah.getDate()
	) {
		year = 1;
	}

	let usia = saiki.getFullYear() - ultah.getFullYear() - year;

	if (usia < 0) {
		usia = 0;
	}

	return usia;
}

function format_tanggal(tanggal) {
	let array = tanggal.split("-");
	let bulan = [
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember",
	];

	if (
		array[2] > 30 &&
		(array[1] == 4 || array[1] == 6 || array[1] == 9 || array[1] == 11)
	) {
		array[2] = 30;
	} else if (array[2] > 30 && parseInt(array[1]) == 2) {
		array[2] = 28;
	}

	let bulan_terpilih = bulan[parseInt(array[1]) - 1];
	return array[2] + " " + bulan_terpilih + " " + array[0];
}

function bulan_depan(tanggal) {
	tanggal = tanggal.split("-");

	let bulan_selanjutnya = parseInt(tanggal[1]) + 1;

	if (bulan_selanjutnya > 12) {
		tanggal[0] = parseInt(tanggal[0]) + 1;
		bulan_selanjutnya = bulan_selanjutnya % 12;
	}

	return tanggal[0] + "-" + bulan_selanjutnya + "-" + tanggal[2];
}

function selisih_hari(date1, date2) {
	let tgl_awal = new Date(date1);
	let tgl_akhir = new Date(date2);

	let hari = 24 * 60 * 60 * 1000;

	let selisih = Math.ceil((tgl_akhir.getTime() - tgl_awal.getTime()) / hari);
	return selisih;
}

function pesan(judul_pesan, isi_pesan) {
	$.gritter.add({
		title: `${judul_pesan}`,
		text: `${isi_pesan}`,
		sticky: false,
	});
}
