-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2020 at 04:01 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `starter`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hak_akses`
--

CREATE TABLE `tbl_hak_akses` (
  `id_hak_akses` int(11) NOT NULL,
  `id_level` varchar(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `jenis_akses` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_hak_akses`
--

INSERT INTO `tbl_hak_akses` (`id_hak_akses`, `id_level`, `id_menu`, `jenis_akses`) VALUES
(254, '4', 5, ''),
(255, '4', 6, ''),
(256, '4', 7, ''),
(284, '1', 12, ''),
(285, '1', 13, ''),
(286, '1', 14, ''),
(287, '1', 16, ''),
(288, '1', 23, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level`
--

CREATE TABLE `tbl_level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_level`
--

INSERT INTO `tbl_level` (`id_level`, `nama_level`) VALUES
(1, 'Admin'),
(2, 'Owner'),
(3, 'Kasir'),
(4, 'Gudang2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(20) NOT NULL,
  `link` varchar(20) DEFAULT NULL,
  `icon` varchar(20) NOT NULL,
  `urutan` tinyint(4) DEFAULT NULL,
  `id_sub_menu` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `link`, `icon`, `urutan`, `id_sub_menu`) VALUES
(4, 'Setting', '', 'fa fa-gears', 4, '0'),
(12, 'Identitas Toko', 'identitas', 'fa fa-building', 4, '4'),
(13, 'Hak Akses Menu', 'hak_akses', 'fa fa-code-fork', 2, '4'),
(14, 'Data Pengguna', 'user', 'fa fa-group', 3, '4'),
(16, 'Manajemen Menu', 'menu', 'fa fa-align-justify', 1, '4'),
(23, 'Level Manajemen', 'level', 'fa fa-crosshairs', 1, '4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(7) NOT NULL,
  `fullname` varchar(50) NOT NULL DEFAULT 'contoh',
  `username` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `alamat` varchar(100) NOT NULL DEFAULT 'desa, kecamatan, kabupaten',
  `email` varchar(50) NOT NULL DEFAULT 'email@contoh.com',
  `no_telp` varchar(12) NOT NULL DEFAULT '08........',
  `img` varchar(100) NOT NULL DEFAULT 'user.jpg',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `id_level` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `fullname`, `username`, `password`, `alamat`, `email`, `no_telp`, `img`, `status`, `id_level`) VALUES
(1, 'Arni Afriani', 'arnia', '7df3497b60a7beede4cda2876f53c98163b4d175ec264731242aabe76d37878c6ccb813adf20d57ff41b4e82247d029bc2dcd1dc6e5b57563a6bc38a1ef18d59XjzXYxjKC1UFZII4A3L6L5Wr3ThldHiq58hU+sUPqmI=', 'kedungbetik, Kesamben Jombang', 'afiantoarfi@gmail.com', '085851132482', 'user.jpg', '1', '3'),
(2, 'Arfi Afianto', 'admin', '0d88eccce7ead12f2a50d1875c9428bc041951b116c67f90d3ee6fd2e1e9978b1ee31703e5bd00277c578c854a08aeb1e06cddcb5d08e39943f52a3ab2c0e95cIbKwwCAXUBcF20vnmIa0ZkLa3SqwPF9IUtulzSWOvXY=', 'Kedungbetik Kesamben Jombang', 'arfiafianto123@gmail.com', '085645327096', 'arfi aa.jpg', '1', '1'),
(3, 'Pegawai Gudang', 'contoh', '0d88eccce7ead12f2a50d1875c9428bc041951b116c67f90d3ee6fd2e1e9978b1ee31703e5bd00277c578c854a08aeb1e06cddcb5d08e39943f52a3ab2c0e95cIbKwwCAXUBcF20vnmIa0ZkLa3SqwPF9IUtulzSWOvXY=', 'desa, kecamatan, kabupaten', 'email@contoh.com', '08.964673', 'avatar-2.jpg', '1', '4'),
(6, 'contoh', 'contoh1', 'fd560b5e79da6c9a976c66cc391991370a1c2016d8292328bb9322758b2d0ed01823112760e61981bae6e4ba5a138ab91bf13f1e66443b3fffd73466fa6bee15CUiT03dfSsd+TK9ey1Eyi2APUu6o10A98JUB1eyayPo=', 'desa, kecamatan, kabupaten', 'email@contoh.com', '08........', 'user.jpg', '1', '2'),
(7, 'Kasir', 'useru', 'a1c07d0ae9b25f5638eeea73ece7ab474523e26c34b1145a7fca25802751c066501934e02596f6039ddf578b37abf57fa097350565f125ba19050d9b7a261e52vmZbllRCiuj/DciQBckyyd+wOWMs1r4qP7BXEwXFj88=', 'desa Kayen, Bandarkedungmulyo, kabupaten', 'arfiafianto123@gmail.com', '08764625114', 'user.jpg', '1', '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_hak_akses`
--
ALTER TABLE `tbl_hak_akses`
  ADD PRIMARY KEY (`id_hak_akses`);

--
-- Indexes for table `tbl_level`
--
ALTER TABLE `tbl_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_hak_akses`
--
ALTER TABLE `tbl_hak_akses`
  MODIFY `id_hak_akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=289;

--
-- AUTO_INCREMENT for table `tbl_level`
--
ALTER TABLE `tbl_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
